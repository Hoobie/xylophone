﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Xylophone.Model;
using Xylophone.Sound;
using Xylophone.Notes;

namespace Xylophone
{
    public partial class Form1 : Form
    {
        private readonly Microphone _mic;
        private readonly List<String> _stringNotes;
        private readonly List<Note> _notes;

        public Form1()
        {
            InitializeComponent();
            _mic = new Microphone();
            _stringNotes = new List<string>();
            _notes = new List<Note>();
            _notes.Add(new C6Note());
            _notes.Add(new D6Note());
            _notes.Add(new E6Note());
            _notes.Add(new F6Note());
            _notes.Add(new G6Note());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _mic.StartRecording();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "" + _mic.StopRecordingAndReturnPitch() + " Hz";
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            _stringNotes.Add("C");
            label2.Text = label2.Text + "C ";
        }

        private void buttonD_Click(object sender, EventArgs e)
        {
            _stringNotes.Add("D");
            label2.Text = label2.Text + "D ";
        }

        private void buttonE_Click(object sender, EventArgs e)
        {
            _stringNotes.Add("E");
            label2.Text = label2.Text + "E ";
        }

        private void buttonF_Click(object sender, EventArgs e)
        {
            _stringNotes.Add("F");
            label2.Text = label2.Text + "F ";
        }

        private void buttonG_Click(object sender, EventArgs e)
        {
            _stringNotes.Add("G");
            label2.Text = label2.Text + "G ";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            XylophoneManager.PlayMelody(_stringNotes);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            label2.Text = "";
            _stringNotes.Clear();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            XylophoneManager.Learn(_mic);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            XylophoneManager.FixC(_mic);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            XylophoneManager.FixD(_mic);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            XylophoneManager.FixE(_mic);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            XylophoneManager.FixF(_mic);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            XylophoneManager.FixG(_mic);
        }

        private void buttonFastC_Click(object sender, EventArgs e)
        {
            _stringNotes.Add("f");
            _stringNotes.Add("C");
            label2.Text = label2.Text + "fC ";
        }

        private void buttonFastD_Click(object sender, EventArgs e)
        {
            _stringNotes.Add("f");
            _stringNotes.Add("D");
            label2.Text = label2.Text + "fD ";
        }

        private void buttonFastE_Click(object sender, EventArgs e)
        {
            _stringNotes.Add("f");
            _stringNotes.Add("E");
            label2.Text = label2.Text + "fE ";
        }

        private void buttonFastF_Click(object sender, EventArgs e)
        {
            _stringNotes.Add("f");
            _stringNotes.Add("F");
            label2.Text = label2.Text + "fF ";
        }

        private void buttonFastG_Click(object sender, EventArgs e)
        {
            _stringNotes.Add("f");
            _stringNotes.Add("G");
            label2.Text = label2.Text + "fG ";
        }

        private void buttonWhistle_Click(object sender, EventArgs e)
        {
            whistle(false);    
        }

        private void button11_Click(object sender, EventArgs e)
        {
            whistle(true);
        }

        private void whistle(bool fast)
        {
            _mic.StartRecording();
            System.Threading.Thread.Sleep(1500);
            float freq = _mic.StopRecordingAndReturnPitch();
            label1.Text = "" + freq + " Hz";
            Note best = null;
            float bestDelta = 20000.0f;
            foreach (Note note in _notes)
            {
                float delta = Math.Abs(freq - note.Frequency);
                if (delta < bestDelta)
                {
                    best = note;
                    bestDelta = delta;
                }
            }
            string name = best.ToString();
            if (fast)
            {
                _stringNotes.Add("f");
                label2.Text = label2.Text + "f" + name + " ";
            }
            else
            {
                label2.Text = label2.Text + name + " ";
            }
            _stringNotes.Add(name);
        }

        private void browseForMelodiesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            int size = -1;
            var openFileDialog = new OpenFileDialog();
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string file = openFileDialog.FileName;
                try
                {
                    string[] lines = File.ReadAllText(file).Split('\n');
                    foreach (string line in lines)
                    {
                        string[] text = line.Split(';');
                        Melody melody = new Melody();
                        melody.Name = text[0];
                        melody.Notes = new List<string>();
                        for (int i = 1; i < text.Length; i++)
                        {
                            melody.Notes.Add(text[i]);
                        }
                        cbMelodies.Items.Add(melody);
                    }
                }
                catch (IOException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void PrepareMelody(object sender, EventArgs e)
        {
            label2.Text = "";
            _stringNotes.Clear();

            var selectedItem = (Melody) cbMelodies.SelectedItem;
            foreach (string note in selectedItem.Notes)
            {
                _stringNotes.Add(note);
                label2.Text = label2.Text + note + " ";
            }
        }
    }
}