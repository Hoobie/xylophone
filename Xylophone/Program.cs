﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Xylophone.Sound;

namespace Xylophone
{
    internal class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
            
            //var mic = new Microphone();
            //XylophoneManager.TestFrequency(mic);
            //XylophoneManager.Learn(mic);
            //XylophoneManager.PlayMelody(new List<string> {"C", "C", "C", "C", "F", "E", "E", "D", "D", "C", "C"});
            //XylophoneManager.PlayMelody(new List<string> {"G", "E", "E", "F", "D", "D", "C", "E", "G", "G", "E", "E", "F", "D", "D", "C", "E", "C"});
            //Console.ReadKey();
        }
    }
}