﻿using System;
using System.Collections.Generic;
using System.Linq;
using NAudio.Wave;

namespace Xylophone.Sound
{
    public class Microphone
    {
        private readonly List<float> _pitchList = new List<float>();
        private readonly WaveFormat _recordingFormat = new WaveFormat(44100, 1);
        private WaveFileWriter _waveFile;
        private WaveInEvent _waveSource;
        private WaveFileWriter _writer;

        public void WriteToFile(byte[] buffer, int bytesRecorded)
        {
            long maxFileLength = _recordingFormat.AverageBytesPerSecond*60;

            var toWrite = (int) Math.Min(maxFileLength - _writer.Length, bytesRecorded);
            if (toWrite > 0)
            {
                try
                {
                    _writer.Write(buffer, 0, bytesRecorded);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                
            }
            else
            {
                _waveSource.StopRecording();
            }
        }

        public void StartRecording()
        {
            _waveSource = new WaveInEvent();
            _waveSource.DeviceNumber = 0;
            _waveSource.DataAvailable += waveSource_DataAvailable;
            _waveSource.RecordingStopped += waveSource_RecordingStopped;
            _waveSource.WaveFormat = _recordingFormat;
            _writer = new WaveFileWriter(@"C:\Temp\Test0001.wav", _recordingFormat);

            _waveSource.StartRecording();
        }

        public float StopRecordingAndReturnPitch()
        {
            _writer.Close();
            _waveSource.StopRecording();
            ApplyAutoTune(@"C:\Temp\Test0001.wav", @"C:\Temp\tmp.wav");
            float query = _pitchList.GroupBy(item => item).OrderByDescending(g => g.Count()).Select(g => g.Key).First();
            _pitchList.Clear();
            return query;
        }

        public void ApplyAutoTune(string fileToProcess, string tempFile)
        {
            using (var reader = new WaveFileReader(fileToProcess))
            {
                IWaveProvider stream32 = new Wave16ToFloatProvider(reader);
                IWaveProvider streamEffect = new FftWaveProvider(stream32, _pitchList);

                // buffer length needs to be a power of 2 for FFT to work nicely
                // however, make the buffer too long and pitches aren't detected fast enough
                // successful buffer sizes: 8192, 4096, 2048, 1024
                // (some pitch detection algorithms need at least 2048)
                var buffer = new byte[4096];
                int bytesRead;
                do
                {
                    bytesRead = streamEffect.Read(buffer, 0, buffer.Length);
                } while (bytesRead != 0);
            }
        }

        private void waveSource_DataAvailable(object sender, WaveInEventArgs e)
        {
            byte[] buffer = e.Buffer;
            int bytesRecorded = e.BytesRecorded;
            WriteToFile(buffer, bytesRecorded);
        }

        private void waveSource_RecordingStopped(object sender, StoppedEventArgs e)
        {
            if (_waveSource != null)
            {
                _waveSource.Dispose();
                _waveSource = null;
            }

            if (_waveFile != null)
            {
                _waveFile.Dispose();
                _waveFile = null;
            }

            // ... and when recording stops we must call Dispose to finalize the
            // .WAV file properly
            _writer.Dispose();
        }
    }
}