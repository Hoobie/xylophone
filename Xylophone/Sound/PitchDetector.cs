﻿using System;

namespace Xylophone.Sound
{
    // FFT based pitch detector. seems to work best with block sizes of 4096
    public class PitchDetector
    {
        private readonly float _sampleRate;
        private float[] _fftBuffer;
        private float[] _prevBuffer;

        public PitchDetector(float sampleRate)
        {
            _sampleRate = sampleRate;
        }

        // http://en.wikipedia.org/wiki/Window_function
        private float HammingWindow(int n, int k)
        {
            return 0.54f - 0.46f*(float) Math.Cos((2*Math.PI*n)/(k - 1));
        }

        public float DetectPitch(float[] buffer, int inFrames)
        {
            Func<int, int, float> window = HammingWindow;
            if (_prevBuffer == null)
            {
                _prevBuffer = new float[inFrames];
            }

            // double frames since we are combining present and previous buffers
            int frames = inFrames*2;
            if (_fftBuffer == null)
            {
                _fftBuffer = new float[frames*2]; // times 2 because it is complex input
            }

            for (int n = 0; n < frames; n++)
            {
                if (n < inFrames)
                {
                    _fftBuffer[n*2] = _prevBuffer[n]*window(n, frames);
                    _fftBuffer[n*2 + 1] = 0; // need to clear out as fft modifies buffer
                }
                else
                {
                    _fftBuffer[n*2] = buffer[n - inFrames]*window(n, frames);
                    _fftBuffer[n*2 + 1] = 0; // need to clear out as fft modifies buffer
                }
            }

            // assuming frames is a power of 2
            SmbPitchShift.smbFft(_fftBuffer, frames, -1);

            float binSize = _sampleRate/frames;
            var minBin = (int) (85/binSize);
            var maxBin = (int) (3000/binSize);
            float maxIntensity = 0f;
            int maxBinIndex = 0;
            for (int bin = minBin; bin <= maxBin; bin++)
            {
                float real = _fftBuffer[bin*2];
                float imaginary = _fftBuffer[bin*2 + 1];
                float intensity = real*real + imaginary*imaginary;
                if (intensity > maxIntensity)
                {
                    maxIntensity = intensity;
                    maxBinIndex = bin;
                }
            }
            return binSize*maxBinIndex;
        }
    }
}