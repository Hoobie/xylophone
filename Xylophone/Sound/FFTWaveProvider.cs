﻿using System;
using System.Collections.Generic;
using NAudio.Wave;

namespace Xylophone.Sound
{
    public class FftWaveProvider : IWaveProvider
    {
        private readonly PitchDetector _pitchDetector;
        private readonly List<float> _pitchList;
        private readonly IWaveProvider _source;
        private const int MaxHold = 1;

        private float _previousPitch;
        private int _release;
        private WaveBuffer _waveBuffer;

        public FftWaveProvider(IWaveProvider source, List<float> pitchList)
        {
            if (source.WaveFormat.SampleRate != 44100)
                throw new ArgumentException("44.1kHz expected!");
            if (source.WaveFormat.Encoding != WaveFormatEncoding.IeeeFloat)
                throw new ArgumentException("Wrong data format, IEEEFloat expected!");
            if (source.WaveFormat.Channels != 1)
                throw new ArgumentException("Too many channels, 1 expected!");

            _source = source;
            _pitchList = pitchList;
            _pitchDetector = new PitchDetector(source.WaveFormat.SampleRate);
            _waveBuffer = new WaveBuffer(4096);
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            if (_waveBuffer == null || _waveBuffer.MaxSize < count)
            {
                _waveBuffer = new WaveBuffer(count);
            }

            int bytesRead = _source.Read(_waveBuffer, 0, count);
            //Debug.Assert(bytesRead == count);

            // the last bit sometimes needs to be rounded up:
            if (bytesRead > 0) bytesRead = count;

            //pitchsource->getPitches();
            int frames = bytesRead/sizeof (float); // MRH: was count
            float pitch = _pitchDetector.DetectPitch(_waveBuffer.FloatBuffer, frames);

            // MRH: an attempt to make it less "warbly" by holding onto the pitch for at least one more buffer
            if (Math.Abs(pitch) < 0.001 && _release < MaxHold)
            {
                pitch = _previousPitch;
                _release++;
            }
            else
            {
                _previousPitch = pitch;
                _release = 0;
            }

            //Console.WriteLine("Frequency: " + pitch);

            _pitchList.Add(pitch);

            return frames*4;
        }

        public WaveFormat WaveFormat
        {
            get { return _source.WaveFormat; }
        }
    }
}