﻿namespace Xylophone
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonD = new System.Windows.Forms.Button();
            this.buttonE = new System.Windows.Forms.Button();
            this.buttonF = new System.Windows.Forms.Button();
            this.buttonG = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.labelSlow = new System.Windows.Forms.Label();
            this.buttonFastG = new System.Windows.Forms.Button();
            this.buttonFastF = new System.Windows.Forms.Button();
            this.buttonFastE = new System.Windows.Forms.Button();
            this.buttonFastD = new System.Windows.Forms.Button();
            this.buttonFastC = new System.Windows.Forms.Button();
            this.labelFast = new System.Windows.Forms.Label();
            this.buttonWhistle = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.cbMelodies = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.browseForMelodiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.browseForMelodiesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 27);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Test Freq Reader";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(261, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 56);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(111, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "StopTest";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 3;
            // 
            // buttonC
            // 
            this.buttonC.Location = new System.Drawing.Point(12, 181);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(28, 23);
            this.buttonC.TabIndex = 4;
            this.buttonC.Text = "C";
            this.buttonC.UseVisualStyleBackColor = true;
            this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
            // 
            // buttonD
            // 
            this.buttonD.Location = new System.Drawing.Point(37, 181);
            this.buttonD.Name = "buttonD";
            this.buttonD.Size = new System.Drawing.Size(28, 23);
            this.buttonD.TabIndex = 6;
            this.buttonD.Text = "D";
            this.buttonD.UseVisualStyleBackColor = true;
            this.buttonD.Click += new System.EventHandler(this.buttonD_Click);
            // 
            // buttonE
            // 
            this.buttonE.Location = new System.Drawing.Point(62, 181);
            this.buttonE.Name = "buttonE";
            this.buttonE.Size = new System.Drawing.Size(28, 23);
            this.buttonE.TabIndex = 7;
            this.buttonE.Text = "E";
            this.buttonE.UseVisualStyleBackColor = true;
            this.buttonE.Click += new System.EventHandler(this.buttonE_Click);
            // 
            // buttonF
            // 
            this.buttonF.Location = new System.Drawing.Point(86, 181);
            this.buttonF.Name = "buttonF";
            this.buttonF.Size = new System.Drawing.Size(28, 23);
            this.buttonF.TabIndex = 8;
            this.buttonF.Text = "F";
            this.buttonF.UseVisualStyleBackColor = true;
            this.buttonF.Click += new System.EventHandler(this.buttonF_Click);
            // 
            // buttonG
            // 
            this.buttonG.Location = new System.Drawing.Point(111, 181);
            this.buttonG.Name = "buttonG";
            this.buttonG.Size = new System.Drawing.Size(28, 23);
            this.buttonG.TabIndex = 9;
            this.buttonG.Text = "G";
            this.buttonG.UseVisualStyleBackColor = true;
            this.buttonG.Click += new System.EventHandler(this.buttonG_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(159, 181);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 10;
            this.button3.Text = "Play Melody";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(240, 180);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 11;
            this.button4.Text = "Clear";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(203, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Freq:";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(26, 97);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 54);
            this.button5.TabIndex = 14;
            this.button5.Text = "Learn!";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(135, 113);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(53, 23);
            this.button6.TabIndex = 15;
            this.button6.Text = "Fix C";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(183, 113);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(53, 23);
            this.button7.TabIndex = 16;
            this.button7.Text = "Fix D";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(233, 113);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(53, 23);
            this.button8.TabIndex = 17;
            this.button8.Text = "Fix E";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(283, 113);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(53, 23);
            this.button9.TabIndex = 18;
            this.button9.Text = "Fix F";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(332, 113);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(53, 23);
            this.button10.TabIndex = 19;
            this.button10.Text = "Fix G";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // labelSlow
            // 
            this.labelSlow.AutoSize = true;
            this.labelSlow.Location = new System.Drawing.Point(9, 165);
            this.labelSlow.Name = "labelSlow";
            this.labelSlow.Size = new System.Drawing.Size(30, 13);
            this.labelSlow.TabIndex = 20;
            this.labelSlow.Text = "Slow";
            // 
            // buttonFastG
            // 
            this.buttonFastG.Location = new System.Drawing.Point(111, 226);
            this.buttonFastG.Name = "buttonFastG";
            this.buttonFastG.Size = new System.Drawing.Size(28, 23);
            this.buttonFastG.TabIndex = 25;
            this.buttonFastG.Text = "G";
            this.buttonFastG.UseVisualStyleBackColor = true;
            this.buttonFastG.Click += new System.EventHandler(this.buttonFastG_Click);
            // 
            // buttonFastF
            // 
            this.buttonFastF.Location = new System.Drawing.Point(86, 226);
            this.buttonFastF.Name = "buttonFastF";
            this.buttonFastF.Size = new System.Drawing.Size(28, 23);
            this.buttonFastF.TabIndex = 24;
            this.buttonFastF.Text = "F";
            this.buttonFastF.UseVisualStyleBackColor = true;
            this.buttonFastF.Click += new System.EventHandler(this.buttonFastF_Click);
            // 
            // buttonFastE
            // 
            this.buttonFastE.Location = new System.Drawing.Point(62, 226);
            this.buttonFastE.Name = "buttonFastE";
            this.buttonFastE.Size = new System.Drawing.Size(28, 23);
            this.buttonFastE.TabIndex = 23;
            this.buttonFastE.Text = "E";
            this.buttonFastE.UseVisualStyleBackColor = true;
            this.buttonFastE.Click += new System.EventHandler(this.buttonFastE_Click);
            // 
            // buttonFastD
            // 
            this.buttonFastD.Location = new System.Drawing.Point(37, 226);
            this.buttonFastD.Name = "buttonFastD";
            this.buttonFastD.Size = new System.Drawing.Size(28, 23);
            this.buttonFastD.TabIndex = 22;
            this.buttonFastD.Text = "D";
            this.buttonFastD.UseVisualStyleBackColor = true;
            this.buttonFastD.Click += new System.EventHandler(this.buttonFastD_Click);
            // 
            // buttonFastC
            // 
            this.buttonFastC.Location = new System.Drawing.Point(12, 226);
            this.buttonFastC.Name = "buttonFastC";
            this.buttonFastC.Size = new System.Drawing.Size(28, 23);
            this.buttonFastC.TabIndex = 21;
            this.buttonFastC.Text = "C";
            this.buttonFastC.UseVisualStyleBackColor = true;
            this.buttonFastC.Click += new System.EventHandler(this.buttonFastC_Click);
            // 
            // labelFast
            // 
            this.labelFast.AutoSize = true;
            this.labelFast.Location = new System.Drawing.Point(9, 214);
            this.labelFast.Name = "labelFast";
            this.labelFast.Size = new System.Drawing.Size(27, 13);
            this.labelFast.TabIndex = 26;
            this.labelFast.Text = "Fast";
            // 
            // buttonWhistle
            // 
            this.buttonWhistle.Location = new System.Drawing.Point(332, 180);
            this.buttonWhistle.Name = "buttonWhistle";
            this.buttonWhistle.Size = new System.Drawing.Size(77, 23);
            this.buttonWhistle.TabIndex = 27;
            this.buttonWhistle.Text = "Whistle";
            this.buttonWhistle.UseVisualStyleBackColor = true;
            this.buttonWhistle.Click += new System.EventHandler(this.buttonWhistle_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(415, 180);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(77, 23);
            this.button11.TabIndex = 28;
            this.button11.Text = "Whistle fast";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // cbMelodies
            // 
            this.cbMelodies.FormattingEnabled = true;
            this.cbMelodies.Location = new System.Drawing.Point(415, 113);
            this.cbMelodies.Name = "cbMelodies";
            this.cbMelodies.Size = new System.Drawing.Size(121, 21);
            this.cbMelodies.TabIndex = 29;
            this.cbMelodies.SelectedIndexChanged += new System.EventHandler(this.PrepareMelody);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.browseForMelodiesToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(570, 24);
            this.menuStrip1.TabIndex = 30;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // browseForMelodiesToolStripMenuItem
            // 
            this.browseForMelodiesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.browseForMelodiesToolStripMenuItem1});
            this.browseForMelodiesToolStripMenuItem.Name = "browseForMelodiesToolStripMenuItem";
            this.browseForMelodiesToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.browseForMelodiesToolStripMenuItem.Text = "File";
            // 
            // browseForMelodiesToolStripMenuItem1
            // 
            this.browseForMelodiesToolStripMenuItem1.Name = "browseForMelodiesToolStripMenuItem1";
            this.browseForMelodiesToolStripMenuItem1.Size = new System.Drawing.Size(181, 22);
            this.browseForMelodiesToolStripMenuItem1.Text = "Browse for melodies";
            this.browseForMelodiesToolStripMenuItem1.Click += new System.EventHandler(this.browseForMelodiesToolStripMenuItem1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(412, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Melodies";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 261);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbMelodies);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.buttonWhistle);
            this.Controls.Add(this.labelFast);
            this.Controls.Add(this.buttonFastG);
            this.Controls.Add(this.buttonFastF);
            this.Controls.Add(this.buttonFastE);
            this.Controls.Add(this.buttonFastD);
            this.Controls.Add(this.buttonFastC);
            this.Controls.Add(this.labelSlow);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.buttonG);
            this.Controls.Add(this.buttonF);
            this.Controls.Add(this.buttonE);
            this.Controls.Add(this.buttonD);
            this.Controls.Add(this.buttonC);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Xylophone";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonD;
        private System.Windows.Forms.Button buttonE;
        private System.Windows.Forms.Button buttonF;
        private System.Windows.Forms.Button buttonG;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Label labelSlow;
        private System.Windows.Forms.Button buttonFastG;
        private System.Windows.Forms.Button buttonFastF;
        private System.Windows.Forms.Button buttonFastE;
        private System.Windows.Forms.Button buttonFastD;
        private System.Windows.Forms.Button buttonFastC;
        private System.Windows.Forms.Label labelFast;
        private System.Windows.Forms.Button buttonWhistle;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.ComboBox cbMelodies;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem browseForMelodiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem browseForMelodiesToolStripMenuItem1;
        private System.Windows.Forms.Label label4;
    }
}