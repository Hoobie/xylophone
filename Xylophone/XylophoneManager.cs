using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using Pololu.UsbWrapper;
using Pololu.Usc;
using Xylophone.Notes;
using Xylophone.Sound;

namespace Xylophone
{
    internal class XylophoneManager
    {
        private static readonly int MoveRightDistance = int.Parse(ConfigurationManager.AppSettings["DistanceBetweenBars"]);
        private const int Step = 25;
        private const int MiddleSleep = 700;
        private static readonly Random Rnd = new Random();

        private static readonly Usc Device = ConnectToDevice();

        private static readonly Servo Servo0 = new Servo(Device, 0);
        private static readonly Servo Servo1 = new Servo(Device, 1);
        private static readonly Servo Servo2 = new Servo(Device, 2);
        private static readonly Servo Servo3 = new Servo(Device, 3);

        private static readonly Note CNote = new C6Note();
        private static readonly Note DNote = new D6Note();
        private static readonly Note ENote = new E6Note();
        private static readonly Note FNote = new F6Note();
        private static readonly Note GNote = new G6Note();

        public static void FixC(Microphone mic)
        {
            LearnVertical(mic, CNote);
        }

        public static void FixD(Microphone mic)
        {
            LearnVertical(mic, DNote);
        }

        public static void FixE(Microphone mic)
        {
            LearnVertical(mic, ENote);
        }

        public static void FixF(Microphone mic)
        {
            LearnVertical(mic, FNote);
        }

        public static void FixG(Microphone mic)
        {
            LearnVertical(mic, GNote);
        }

        public static void Learn(Microphone mic)
        {
            LearnVertical(mic, CNote);
            Array.Copy(CNote.StartPositions, DNote.StartPositions, 4);
            Array.Copy(CNote.TargetPositions, DNote.TargetPositions, 4);
            MoveRight(DNote);
            LearnVertical(mic, DNote);
            Array.Copy(DNote.StartPositions, ENote.StartPositions, 4);
            Array.Copy(DNote.TargetPositions, ENote.TargetPositions, 4);
            MoveRight(ENote);
            LearnVertical(mic, ENote);
            Array.Copy(ENote.StartPositions, FNote.StartPositions, 4);
            Array.Copy(ENote.TargetPositions, FNote.TargetPositions, 4);
            MoveRight(FNote);
            LearnVertical(mic, FNote);
            Array.Copy(FNote.StartPositions, GNote.StartPositions, 4);
            Array.Copy(FNote.TargetPositions, GNote.TargetPositions, 4);
            MoveRight(GNote);
            LearnVertical(mic, GNote);
        }

        private static void MoveRight(Note note)
        {
            note.StartPositions[0] += MoveRightDistance;
            note.TargetPositions[0] += MoveRightDistance;
        }

        private static void LearnVertical(Microphone mic, Note note)
        {
            Servo0.TrySetTarget(note.StartPositions[0]);
            Servo1.TrySetTarget(note.StartPositions[1]);
            Servo2.TrySetTarget(note.StartPositions[2]);
            Servo3.TrySetTarget(note.StartPositions[3]);

            const double alpha = 0.95;
            const double epsilon = 0.1;
            const double freqEpsilon = 15;
            double prevFrequencyDelta = 20000.0;
            double temperature = 25.0;
            int next = note.StartPositions[2];

            while (temperature > epsilon || prevFrequencyDelta > freqEpsilon)
            {
                Thread.Sleep(1000);

                mic.StartRecording();

                Hit(note, 600, 120, 1400);

                float newFrequency = mic.StopRecordingAndReturnPitch();

                next = ComputeNext(next);

                double frequencyDelta = Math.Abs(note.Frequency - newFrequency);

                if (frequencyDelta < prevFrequencyDelta)
                {
                    note.TargetPositions[2] = next;
                    prevFrequencyDelta = frequencyDelta;
                    Console.WriteLine("Better!");
                }
                else
                {
                    double probability = Rnd.Next(100);
                    if (probability < 25 && temperature > 10)
                    {
                        note.TargetPositions[2] = next;
                        prevFrequencyDelta = frequencyDelta;
                        Console.WriteLine("Worse taken!");
                    }
                }
                temperature *= alpha;

                Console.WriteLine("==========================");
                Console.WriteLine("Note: " + note);
                Console.WriteLine("Frequency: " + newFrequency);
                Console.WriteLine("Servo 2: " + next);
                Console.WriteLine("Temperature: " + temperature);
            }
            
            if (temperature < epsilon)
            {
                Console.WriteLine("Learn failure.");
            }
        }

        private static int _change = -1;
        private static bool _minReached = true;
        private static bool _maxReached = false;
        private static int ComputeNext(int currPos)
        {
            int next = currPos;
            while (next == currPos)
            {
                if (_maxReached && next > int.Parse(ConfigurationManager.AppSettings["MinimalHitHeight"]))
                {
                    _minReached = true;
                    _maxReached = false;
                    _change = -1;
                }
                if (_minReached && next < int.Parse(ConfigurationManager.AppSettings["MaximalHitHeight"]))
                {
                    _minReached = false;
                    _maxReached = true;
                    _change = 1;
                }
                next = next + _change*(Rnd.Next(Step) + 1);
            }

            return next;
        }

        public static void PlayMelody(List<string> list)
        {
            bool fast = false;
            foreach (string strnote in list)
            {
                Note note;
                switch (strnote)
                {
                    case "f":
                        fast = true;
                        continue;
                    case "C":
                        note = CNote;
                        break;
                    case "D":
                        note = DNote;
                        break;
                    case "E":
                        note = ENote;
                        break;
                    case "F":
                        note = FNote;
                        break;
                    case "G":
                        note = GNote;
                        break;
                    default:
                        note = CNote;
                        break;
                }
                HitMelody(note, fast ? MiddleSleep/2 : MiddleSleep, 150);
                fast = false;
            }
        }

        private static void Hit(Note note, int beginSleep, int middleSleep, int endSleep)
        {
            Thread.Sleep(beginSleep);

            Servo0.TrySetTarget(note.TargetPositions[0]);
            Servo1.TrySetTarget(note.TargetPositions[1]);
            Servo2.TrySetTarget(note.TargetPositions[2]);
            Servo3.TrySetTarget(note.TargetPositions[3]);

            Thread.Sleep(middleSleep);

            Servo0.TrySetTarget(note.StartPositions[0]);
            Servo1.TrySetTarget(note.StartPositions[1]);
            Servo2.TrySetTarget(note.StartPositions[2]);
            Servo3.TrySetTarget(note.StartPositions[3]);

            Thread.Sleep(endSleep);
        }

        private static void HitMelody(Note note, int middleSleep, int endSleep)
        {
            Servo0.TrySetTarget(note.StartPositions[0]);
            Servo1.TrySetTarget(note.StartPositions[1]);
            Servo2.TrySetTarget(note.StartPositions[2]);
            Servo3.TrySetTarget(note.StartPositions[3]);

            Thread.Sleep(middleSleep);

            Servo0.TrySetTarget(note.TargetPositions[0]);
            Servo1.TrySetTarget(note.TargetPositions[1]);
            Servo2.TrySetTarget(note.TargetPositions[2]);
            Servo3.TrySetTarget(note.TargetPositions[3]);

            Thread.Sleep(endSleep);
        }

        private static void TestFrequency(Microphone mic)
        {
            mic.StartRecording();
            Console.ReadKey();
            Console.WriteLine("Frequency: " + mic.StopRecordingAndReturnPitch());
        }

        /// <summary>
        ///     Connects to a Maestro using native USB and returns the Usc object
        ///     representing that connection.  When you are done with the
        ///     connection, you should close it using the Dispose() method so that
        ///     other processes or functions can connect to the device later.  The
        ///     "using" statement can do this automatically for you.
        /// </summary>
        private static Usc ConnectToDevice()
        {
            // Get a list of all connected devices of this type.
            List<DeviceListItem> connectedDevices = Usc.getConnectedDevices();

            foreach (DeviceListItem dli in connectedDevices)
            {
                // If you have multiple devices connected and want to select a particular
                // device by serial number, you could simply add a line like this:
                //   if (dli.serialNumber != "00012345"){ continue; }

                var device = new Usc(dli); // Connect to the device.
                return device; // Return the device.
            }
            throw new Exception("Could not find device.  Make sure it is plugged in to USB " +
                                "and check your Device Manager (Windows) or run lsusb (Linux).");
        }
    }
}