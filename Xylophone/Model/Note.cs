﻿using System;
using System.Configuration;

namespace Xylophone.Notes
{
    public abstract class Note
    {
        public int Frequency { get; set; }
        public int[] StartPositions { get; set; }
        public int[] TargetPositions { get; set; }

        protected Note(int freq)
        {
            Frequency = freq;
            StartPositions = new[]
            {
                int.Parse(ConfigurationManager.AppSettings["Servo0InitPos"]),
                int.Parse(ConfigurationManager.AppSettings["Servo1InitPos"]),
                int.Parse(ConfigurationManager.AppSettings["Servo2InitPos"]),
                int.Parse(ConfigurationManager.AppSettings["Servo3InitPos"]),
            };
            TargetPositions = new[] 
            {
                int.Parse(ConfigurationManager.AppSettings["Servo0InitPos"]),
                int.Parse(ConfigurationManager.AppSettings["Servo1InitPos"]),
                int.Parse(ConfigurationManager.AppSettings["Servo2InitPos"]) + int.Parse(ConfigurationManager.AppSettings["InitHitHeight"]),
                int.Parse(ConfigurationManager.AppSettings["Servo3InitPos"]),
            };
        }

        public abstract string ToString();
    }
}