﻿namespace Xylophone.Notes
{
    public class G6Note : Note
    {
        public G6Note() : base(1568)
        {
        }

        public override string ToString()
        {
            return "G";
        }
    }
}