﻿namespace Xylophone.Notes
{
    public class E6Note : Note
    {
        public E6Note() : base(1319)
        {
        }

        public override string ToString()
        {
            return "E";
        }
    }
}