﻿namespace Xylophone.Notes
{
    public class F6Note : Note
    {
        public F6Note() : base(1397)
        {
        }

        public override string ToString()
        {
            return "F";
        }
    }
}