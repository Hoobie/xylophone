﻿namespace Xylophone.Notes
{
    public class C6Note : Note
    {
        public C6Note() : base(1047)
        {
        }

        public override string ToString()
        {
            return "C";
        }
    }
}