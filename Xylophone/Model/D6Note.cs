﻿namespace Xylophone.Notes
{
    public class D6Note : Note
    {
        public D6Note() : base(1175)
        {
        }

        public override string ToString()
        {
            return "D";
        }
    }
}