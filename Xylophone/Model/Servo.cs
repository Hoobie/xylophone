﻿using Pololu.Usc;

namespace Xylophone
{
    internal class Servo
    {
        public Servo(Usc device, byte number)
        {
            Device = device;
            Number = number;
            //device.setSpeed(number, 150);
            //device.setAcceleration(number, 150);
        }

        private Usc Device { get; set; }
        private byte Number { get; set; }
        private int Position { get; set; }

        /// <summary>
        ///     Attempts to set the target (width of pulses sent) of a channel.
        /// </summary>
        /// <param name="channel">Channel number from 0 to 23.</param>
        /// <param name="target">
        ///     Target, in units of quarter microseconds.  For typical servos,
        ///     6000 is neutral and the acceptable range is 4000-8000.
        /// </param>
        public void TrySetTarget(int target)
        {
            Device.setTarget(Number, (ushort) (target*4));
        }
    }
}